package org.bjtu.word2vec;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.edu.bjtu.listener.AppContextListener;
import groovy.lang.Tuple2;




public class ApplicationConfig extends Properties{
	private static final Log LOG = LogFactory.getLog(ApplicationConfig.class);
	private ApplicationConfig(){
		try {
		this.load(AppContextListener.class.getResourceAsStream("/appConfig.properties"));
		} catch (IOException e) {
			LOG.error("Loading config file is fault");
		}	
	}
	private static final long serialVersionUID = 1639498280356484991L;
	public static ApplicationConfig getInstance(){
		if(instance == null){
			synchronized (ApplicationConfig.class) {
				if(instance == null){
					instance = new ApplicationConfig();
				}
			}
		}
		return instance;
	}
	
	private static ApplicationConfig instance = null;
}
