package org.bjtu.word2vec;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.h2.store.fs.FilePath;

import cn.edu.bjtu.tools.FileInfo;
import cn.edu.bjtu.tools.FileUtil;
import cn.edu.bjtu.tools.ThreadLocalDateUtil;
import cn.edu.bjtu.tools.message.Message;
import net.sf.json.JSONObject;

/**
 * 
 * @author liyao 2017-05-02 基础控制类
 */
public abstract class BaseController {
	/**
	 * 获取训练文件信息
	 * 
	 * @return
	 */
	protected List<FileInfo> getFileInfo(File path) {
		List<FileInfo> listFileInfo = new ArrayList<FileInfo>();
		List<File> filelist =new ArrayList<File>();
		if (path.isDirectory()) {
			FileUtil.getFilepath(path, filelist);
			for (File file : filelist) {
				try {
					if (file.exists()) {
						// 获取文件大小
						FileChannel fc = null;
						FileInputStream fis;
						fis = new FileInputStream(file);
						fc = fis.getChannel();
						long size = fc.size();
						String fileName = file.getName();// 获取文件名
						// 获取文件最近修改日期
						String modifyDate = ThreadLocalDateUtil.getDateFormat().format(file.lastModified());
						// 作为对象返回
						FileInfo fileInfo = new FileInfo(fileName, size, modifyDate);
						listFileInfo.add(fileInfo);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			if (path.exists()) {
				try {
					// 获取文件大小
					FileChannel fc = null;
					FileInputStream fis;
					fis = new FileInputStream(path);
					fc = fis.getChannel();
					long size = fc.size();// 获取文件名
					String fileName = path.getName();
					// 获取文件最近修改日期
					String modifyDate = ThreadLocalDateUtil.getDateFormat().format(path.lastModified());
					// 作为对象返回
					FileInfo fileInfo = new FileInfo(fileName, size, modifyDate);
					listFileInfo.add(fileInfo);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return listFileInfo;
	}
	public static void main(String[] args) {
//		BaseController bc = new BaseController();
//		bc.getFileInfo(new File("/media/liyao/ubuntu/userData324"));
	}
	
	protected <C extends Message> C getReturnMessage(int status,String message,Class<C> clz){
		try {
			Constructor<C> ctors = clz.getConstructor(int.class,String.class);
			C res = ctors.newInstance(status,message);
			return res;
		} catch (Exception e) {e.printStackTrace();}
		return null;
	}
	protected String dumpJSON(Object o){
		return JSONObject.fromObject(o).toString();
	}
	
}
