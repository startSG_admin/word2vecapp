/**
 * 
 */
package cn.edu.bjtu.model.dl4jdoc2vec;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.bytedeco.javacpp.opencv_core.LDA;
import org.datavec.api.records.reader.impl.jackson.JacksonRecordReader;
import org.datavec.api.records.reader.impl.misc.LibSvmRecordReader;

import cn.edu.bjtu.general.math.Vector;
import cn.edu.bjtu.interfaces.ILearnerModel;
import cn.edu.bjtu.interfaces.ModelStatus;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.vector.IDocumentVector;
import cn.edu.bjtu.interfaces.wordfilters.IWord;
/**
 * @author alex
 *
 */
public class DL4jDoc2VecModel implements ILearnerModel{
	@Override
	public void fit(Iterator<IDocument> docs) {
		JacksonRecordReader jrr;
		LibSvmRecordReader lsrr;
	}
	@Override
	public IDocumentVector transform(IDocument doc) {
		return null;
	}

	@Override
	public List<IDocumentVector> transform(Iterator<IDocument> docs) {

		return null;
	}

	@Override
	public Vector queryWord(IWord word) {
		return null;
	}

	@Override
	public Iterator<IWord> getTopKSimilar(int k, IWord word) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean isModelExist() {
		return false;
	}
	@Override
	public ModelStatus loadModelFromFile(File file) {
		return null;
	}

}
