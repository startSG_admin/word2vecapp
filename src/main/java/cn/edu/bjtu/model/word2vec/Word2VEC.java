package cn.edu.bjtu.model.word2vec;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import cn.edu.bjtu.model.word2vec.domain.WordEntry;

import java.util.Set;
import java.util.TreeSet;


public class Word2VEC {

	public static void main(String[] args) throws IOException {

		long start = System.currentTimeMillis();
		 Learn learn = new Learn();
		 learn.learnFile(new File("/home/liyao/tools/TBD/word2vecFile/testFile.txt"));
		 learn.saveModel(new File("/home/liyao/tools/TBD/word2vecFile/javaSkip1"));

		Word2VEC vec = new Word2VEC();
		vec.loadJavaModel("/home/liyao/tools/TBD/word2vecFile/javaSkip1");

		// System.out.println("中国" + "\t" +
		// Arrays.toString(vec.getWordVector("中国")));
		// ;
		// System.out.println("毛泽东" + "\t" +
		// Arrays.toString(vec.getWordVector("毛泽东")));
		// ;
		// System.out.println("足球" + "\t" +
		// Arrays.toString(vec.getWordVector("足球")));

		// Word2VEC vec2 = new Word2VEC();
		// vec2.loadGoogleModel("library/vectors.bin") ;
		//
		//
//		String str = "毛泽东";
//		System.out.println(Arrays.toString(vec.getWordVector(str)));
////	     System.out.println(vec.distance(str));
////		System.out.println(System.currentTimeMillis() - start);

		System.out.println(System.currentTimeMillis() - start);
		// System.out.println(vec2.distance(str));
		//
		//
		// //男人 国王 女人
		// System.out.println(vec.analogy("邓小平", "毛泽东思想", "毛泽东"));
		// System.out.println(vec2.analogy("毛泽东", "毛泽东思想", "邓小平"));
		double[] a=new double[]{ 0.07440282, -0.054397937, -0.054947186, 0.044009384, 0.047042873, -0.05082304, 0.09350507, -0.06746111, 0.11372277, 0.08194942, 0.011118964, 0.0052999803, 0.05383047, 0.110181905, 0.062170263, -0.038081635, 0.11164639, -0.13798156, 5.532703E-4, -0.06488718, -0.03399912, 0.052207932, 0.12204512, 0.021094617, -0.043978896, 0.13080847, 0.0047170543, -0.026028855, 0.099381626, 0.025786381, -0.053870752, 0.010431739, -0.02701523, 0.053396728, 0.08537464, -0.009823187, 0.050092738, 0.09300325, -0.029882507, -0.0212544, -0.033717524, -0.05448852, 0.074184135, -0.13095915, -0.121534266, -0.011565795, 0.06367247, 0.028259568, -0.0057500745, -0.006334626, 0.021230515, -0.020485466, -0.024579197, 0.059736043, -0.09474997, -0.12001042, 0.051007785, 0.016644124, -0.051458124, -0.09229913, -0.046610013, -0.07448945, 0.019040884, 0.033019602, 0.011064708, 0.085473016, 0.059967395, -0.018180354, 0.053508557, 0.0093281325, 0.08509824, 0.11349785, -0.070747115, -0.046042353, 0.05218155, 0.03568071, 0.012190018, 0.061775003, -0.013994865, 0.030875714, 0.17946747, -0.050574727, 0.016798943, -0.08805158, -0.0837309, 0.001114966, -0.008455454, 0.006944132, 0.041426867, 0.042470288, -0.107523404, -0.027148595, -9.929474E-4, 0.012512166, 0.13913913, 0.11492136, -0.024579773, -1.7295589E-4, 0.1898142, -0.058169913, -0.0054043336, 0.1292525, 0.10670442, 0.019265562, -0.19715282, 0.06269165, 0.06847777, -0.07496315, 0.011628261, -0.06484498, 0.104747884, 0.08757916, 0.12902601, 0.03972381, 0.096395425, -0.08716161, -0.025484273, -0.0054912926, 0.06337563, -0.023162464, 0.071888, 0.05572611, 0.05769692, -0.09742123, 0.038648605, 0.031962104, -0.059310265, 0.03269388, -0.0497844, 0.09954656, -0.0013807729, 0.045022584, -0.10228004, 0.055459112, -0.09745559, 0.06188212, 0.029094484, -0.052881952, 0.13326904, -7.0125255E-4, 0.045618493, 0.051710468, 0.08781163, -0.026692538, 0.020973053, -0.032697108, 0.07739253, -0.07356109, 0.008656305, 0.07045121, 0.065065876, -0.060236067, 0.10552231, 0.04448683, -0.015358242, 0.042990405, 0.06227935, -0.07775228, -0.16738404, 0.01668784, 0.026875095, -0.021528563, -0.12074305, 0.07442327, -0.069799975, 0.06625788, 0.06683596, -0.111874275, -0.039732233, 0.008990792, 0.036773715, -0.03980246, -0.031686835, -0.08587271, 0.13219945, 0.029090973, 0.042447533, -0.009410578, 0.02120725, -0.07304253, -0.036730397, 0.041813, 0.020794326, -0.06450567, 0.044033658, 0.08646658, -0.14679971, -0.033952244, -0.11344545, -0.006502247, 0.004566258, 0.058578435, 0.08574358, -0.053838097, 0.1036833, 0.049020004, -0.07842758, 0.14610288, 0.077935725, -0.03642473};
System.out.println(a.length);
	}

	private HashMap<String, float[]> wordMap = new HashMap<String, float[]>();

	private int words;
	private int size;
	private int topNSize = 40;

	/**
	 * 加载模型
	 * 
	 * @param path
	 *            模型的路径
	 * @throws IOException
	 */
	public void loadGoogleModel(String path) throws IOException {
		DataInputStream dis = null;
		BufferedInputStream bis = null;
		double len = 0;
		float vector = 0;
		try {
			bis = new BufferedInputStream(new FileInputStream(path));
			dis = new DataInputStream(bis);
			// //读取词数
			words = Integer.parseInt(readString(dis));
			// //大小
			size = Integer.parseInt(readString(dis));
			String word;
			float[] vectors = null;
			for (int i = 0; i < words; i++) {
				word = readString(dis);
				vectors = new float[size];
				len = 0;
				for (int j = 0; j < size; j++) {
					vector = readFloat(dis);
					len += vector * vector;
					vectors[j] = (float) vector;
				}
				len = Math.sqrt(len);

				for (int j = 0; j < size; j++) {
					vectors[j] /= len;
				}

				wordMap.put(word, vectors);
				dis.read();
			}
		} finally {
			bis.close();
			dis.close();
		}
	}

	/**
	 * 加载模型
	 * 
	 * @param path
	 *            模型的路径
	 * @throws IOException
	 */
	public boolean loadJavaModel(String path) throws IOException {
		boolean exist = false;
		try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(path)))) {
			words = dis.readInt();
			size = dis.readInt();

			float vector = 0;

			String key = null;
			float[] value = null;
			for (int i = 0; i < words; i++) {
				double len = 0;
				key = dis.readUTF();
				value = new float[size];
				for (int j = 0; j < size; j++) {
					vector = dis.readFloat();
					len += vector * vector;
					value[j] = vector;
				}

				len = Math.sqrt(len);

				for (int j = 0; j < size; j++) {
					value[j] /= len;
				}
				wordMap.put(key, value);
			}
          exist = true;
		}catch(Exception ex){
			exist = false;
		}
		return exist;
	}

	private static final int MAX_SIZE = 50;

	/**
	 * 近义词
	 * 
	 * @return
	 */
	public TreeSet<WordEntry> analogy(String word0, String word1, String word2) {
		float[] wv0 = getWordVector(word0);
		float[] wv1 = getWordVector(word1);
		float[] wv2 = getWordVector(word2);

		if (wv1 == null || wv2 == null || wv0 == null) {
			return null;
		}
		float[] wordVector = new float[size];
		for (int i = 0; i < size; i++) {
			wordVector[i] = wv1[i] - wv0[i] + wv2[i];
		}
		float[] tempVector;
		String name;
		List<WordEntry> wordEntrys = new ArrayList<WordEntry>(topNSize);
		for (Entry<String, float[]> entry : wordMap.entrySet()) {
			name = entry.getKey();
			if (name.equals(word0) || name.equals(word1) || name.equals(word2)) {
				continue;
			}
			float dist = 0;
			tempVector = entry.getValue();
			for (int i = 0; i < wordVector.length; i++) {
				dist += wordVector[i] * tempVector[i];
			}
			insertTopN(name, dist, wordEntrys);
		}
		return new TreeSet<WordEntry>(wordEntrys);
	}

	private void insertTopN(String name, float score, List<WordEntry> wordsEntrys) {
		// TODO Auto-generated method stub
		if (wordsEntrys.size() < topNSize) {
			wordsEntrys.add(new WordEntry(name, score));
			return;
		}
		float min = Float.MAX_VALUE;
		int minOffe = 0;
		for (int i = 0; i < topNSize; i++) {
			WordEntry wordEntry = wordsEntrys.get(i);
			if (min > wordEntry.score) {
				min = wordEntry.score;
				minOffe = i;
			}
		}

		if (score > min) {
			wordsEntrys.set(minOffe, new WordEntry(name, score));
		}

	}

	public Set<WordEntry> distance(String queryWord) {

		float[] center = wordMap.get(queryWord);
		if (center == null) {
			return Collections.emptySet();
		}

		int resultSize = wordMap.size() < topNSize ? wordMap.size() : topNSize;
		TreeSet<WordEntry> result = new TreeSet<WordEntry>();

		double min = Float.MIN_VALUE;
		for (Map.Entry<String, float[]> entry : wordMap.entrySet()) {
			float[] vector = entry.getValue();
			float dist = 0;
			for (int i = 0; i < vector.length; i++) {
				dist += center[i] * vector[i];
			}

			if (dist > min) {
				result.add(new WordEntry(entry.getKey(), dist));
				if (resultSize < result.size()) {
					result.pollLast();
				}
				min = result.last().score;
			}
		}
		result.pollFirst();

		return result;
	}

	public Set<WordEntry> distance(List<String> words) {

		float[] center = null;
		for (String word : words) {
			center = sum(center, wordMap.get(word));
		}

		if (center == null) {
			return Collections.emptySet();
		}

		int resultSize = wordMap.size() < topNSize ? wordMap.size() : topNSize;
		TreeSet<WordEntry> result = new TreeSet<WordEntry>();

		double min = Float.MIN_VALUE;
		for (Map.Entry<String, float[]> entry : wordMap.entrySet()) {
			float[] vector = entry.getValue();
			float dist = 0;
			for (int i = 0; i < vector.length; i++) {
				dist += center[i] * vector[i];
			}

			if (dist > min) {
				result.add(new WordEntry(entry.getKey(), dist));
				if (resultSize < result.size()) {
					result.pollLast();
				}
				min = result.last().score;
			}
		}
		result.pollFirst();

		return result;
	}

	private float[] sum(float[] center, float[] fs) {
		// TODO Auto-generated method stub

		if (center == null && fs == null) {
			return null;
		}

		if (fs == null) {
			return center;
		}

		if (center == null) {
			return fs;
		}

		for (int i = 0; i < fs.length; i++) {
			center[i] += fs[i];
		}

		return center;
	}

	/**
	 * 得到词向量
	 * 
	 * @param word
	 * @return
	 */
	public float[] getWordVector(String word) {
		return wordMap.get(word);
	}

	public static float readFloat(InputStream is) throws IOException {
		byte[] bytes = new byte[4];
		is.read(bytes);
		return getFloat(bytes);
	}

	/**
	 * 读取一个float
	 * 
	 * @param b
	 * @return
	 */
	public static float getFloat(byte[] b) {
		int accum = 0;
		accum = accum | (b[0] & 0xff) << 0;
		accum = accum | (b[1] & 0xff) << 8;
		accum = accum | (b[2] & 0xff) << 16;
		accum = accum | (b[3] & 0xff) << 24;
		return Float.intBitsToFloat(accum);
	}

	/**
	 * 读取一个字符串
	 * 
	 * @param dis
	 * @return
	 * @throws IOException
	 */
	private static String readString(DataInputStream dis) throws IOException {
		// TODO Auto-generated method stub
		byte[] bytes = new byte[MAX_SIZE];
		byte b = dis.readByte();
		int i = -1;
		StringBuilder sb = new StringBuilder();
		while (b != 32 && b != 10) {
			i++;
			bytes[i] = b;
			b = dis.readByte();
			if (i == 49) {
				sb.append(new String(bytes));
				i = -1;
				bytes = new byte[MAX_SIZE];
			}
		}
		sb.append(new String(bytes, 0, i + 1));
		return sb.toString();
	}

	public int getTopNSize() {
		return topNSize;
	}

	public void setTopNSize(int topNSize) {
		this.topNSize = topNSize;
	}

	public HashMap<String, float[]> getWordMap() {
		return wordMap;
	}

	public int getWords() {
		return words;
	}

	public int getSize() {
		return size;
	}

}
