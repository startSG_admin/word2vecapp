package cn.edu.bjtu.model.word2vec.util;

import cn.edu.bjtu.interfaces.wordfilters.IWord;

public class WordInWord2Vec implements IWord {
	private String word;
	private double weight;

	public WordInWord2Vec(String word, double weight) {
		this.word = word;
		this.weight = weight;
	}

	public WordInWord2Vec(String word) {
		super();
		this.word = word;
	}

	public String getWord() {
		return this.word;
	}

	public double getWeight() {
		return this.weight;
	}
}
