package cn.edu.bjtu.model.dl4jword2vec;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bjtu.word2vec.ApplicationConfig;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.models.embeddings.WeightLookupTable;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.AbstractCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.bjtu.abstractimpl.analyzer.AnsjDocumentAnalyzer;
import cn.edu.bjtu.abstractimpl.wordfilters.RegExNumbefFilter;
import cn.edu.bjtu.abstractimpl.wordfilters.SingleWordTermFilter;
import cn.edu.bjtu.classimpl.Dl4ModelStatus;
import cn.edu.bjtu.classimpl.documententity.Document;
import cn.edu.bjtu.classimpl.documentvec.DocumentVectorImpl;
import cn.edu.bjtu.classimpl.parser.DataBaseWechatParser;
import cn.edu.bjtu.classimpl.parser.FDDataParser;
import cn.edu.bjtu.classimpl.parser.HiveWechatParser;
import cn.edu.bjtu.classimpl.parser.LineParser;
import cn.edu.bjtu.classimpl.parser.doscParser;
import cn.edu.bjtu.general.math.DenseVector;
import cn.edu.bjtu.general.math.Vector;
import cn.edu.bjtu.interfaces.ModelStatus;
import cn.edu.bjtu.interfaces.ILearnerModel;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.parser.Parser;
import cn.edu.bjtu.interfaces.segment.DocumentSegmentation;
import cn.edu.bjtu.interfaces.vector.IDocumentVector;
import cn.edu.bjtu.interfaces.wordfilters.IWord;
import cn.edu.bjtu.interfaces.wordfilters.WordsFilter;
import cn.edu.bjtu.io.WechatLineSentenceRecordReader;
import cn.edu.bjtu.model.word2vec.Word2VEC;
import cn.edu.bjtu.model.word2vec.Word2VecForTransformingDocs;
import cn.edu.bjtu.model.word2vec.util.WordInWord2Vec;
import cn.edu.bjtu.tokenization.AnsjTokenPreprocessor;
import cn.edu.bjtu.tokenization.AnsjTokenzierFactory;
import cn.edu.bjtu.tools.FileInfo;
import cn.edu.bjtu.tools.FileUtil;
import cn.edu.bjtu.tools.ModelInfo;
import cn.edu.bjtu.tools.ThreadLocalDateUtil;
import cn.edu.bjtu.tools.message.DocumentVectorMessage;
import cn.edu.bjtu.tools.message.Message;
import lombok.NonNull;
import net.sf.json.JSONObject;

/**
 * 
 * @author LiYao 2017-04-29
 *
 */
public class DL4jWordVecModel implements ILearnerModel {
	private static Logger LOG = LoggerFactory
			.getLogger(DL4jWordVecModel.class);
	// 词向量长度
	private int vector_length = 100;
	// 最小词频数（低于此的词过滤）
	private int min_word_frequency = 5;

	private int windows_size = 5;
	// number of dimensions for output
	private int layer_size = 100;
	// random seed for random numbers generator
	private static int seed = 42;
	// 迭代次数
	private static int epochs = 1;
	// 分词
	private DocumentSegmentation docSeg = null;
	// 词过滤器
	private Set<WordsFilter> filtersSet = new HashSet<WordsFilter>();
	private static DL4jWordVecModel mDl4jWordVecModel = null;
	AtomicLong cur = new AtomicLong(0L);
	// dl4j模型
	private Word2Vec word2Vec = null;
	// 模型文件路径
	private String DL4JMODEL;
	// 训练文件路径
	private String TRAIN_FILE;
	// 转换文件路径
	private String TRNSFORMFILE;
	// 转换文件夹路径
	private String TRANSFORM_DIR;
	// 训练数据转换
	private String TRANFILE_TRANSOFRM;

	// 训练文件预处理文件（分词）
	private String TRAINFILE_PROCESS;

	private ModelStatus loadModelStatus;

	// 模型锁
	private ReadWriteLock modelRWLock = new ReentrantReadWriteLock();

	// 文档转换锁
	private ReadWriteLock transformLock = new ReentrantReadWriteLock();

	/**
	 * Parameter settings for the training process
	 */
	private Map<String, Integer> options = new HashMap<String, Integer>();

	// 单例
	public static DL4jWordVecModel getInstance() {
		if (mDl4jWordVecModel == null) {
			mDl4jWordVecModel = new DL4jWordVecModel();
		}
		return mDl4jWordVecModel;
	}

	// 私有构造
	private DL4jWordVecModel() {
		// 词过滤器设置
		filtersSet.add(new SingleWordTermFilter());
		filtersSet.add(new RegExNumbefFilter());
		// 默认使用ansj的分词工具
		docSeg = new AnsjDocumentAnalyzer();
		// 参数初始化设置
		setOptions();
	}

	// 参数设置
	private void setOptions() {
		TRAIN_FILE = ApplicationConfig.getInstance()
				.getProperty("dl4j_trainfile");
		TRANSFORM_DIR = ApplicationConfig.getInstance()
				.getProperty("transform_dir");
		TRANFILE_TRANSOFRM = ApplicationConfig.getInstance()
				.getProperty("dl4j_trainfile_tranform");
		TRAINFILE_PROCESS = ApplicationConfig.getInstance()
				.getProperty("dl4j_trainfile_process");
		DL4JMODEL = ApplicationConfig.getInstance()
				.getProperty("word2vec_file");
	}

	/**
	 * @param lerningFile
	 *            训练文件路径，使用默认的参数来训练模型
	 * @return
	 */
	public ModelStatus builderModel(String trainFile) {
		try {
			modelRWLock.writeLock().lock();
			boolean result;
			ModelStatus builder;
			if (JudgeFileExist(TRAIN_FILE) || trainFile != null) {
				long start = System.currentTimeMillis();
				if (trainFile != null) {
					result = builderModel(trainFile, null);
				} else {
					result = builderModel(TRAIN_FILE, null);
				}
				long end = System.currentTimeMillis();
				if (result)
					builder = new Dl4ModelStatus(1, (end - start)); // 建模成功
				else {
					builder = new Dl4ModelStatus(-1); // 建模失败
				}

			} else {
				builder = new Dl4ModelStatus(0); // 训练文件不存在
			}
			return builder;
		} finally {
			modelRWLock.writeLock().unlock();
		LOG.info("训练模型过程释放锁");
		}

	}

	/**
	 * 
	 * @param lerningFilePath
	 *            训练文件路径
	 * @param options
	 *            基本参数设置
	 * @return
	 * @throws InterruptedException
	 */
	public boolean builderModel(@NonNull String lerningFilePath,
			Map<String, Integer> paraOptions) {
		try {

			DL4JMODEL = ApplicationConfig.getInstance()
					.getProperty("word2vec_model_dir") + layer_size
					+ "_" + epochs + ".txt";
			LOG.info("Loading train file.....");
			FileSplit fs = new FileSplit(new File(lerningFilePath));
			WechatLineSentenceRecordReader witer = new WechatLineSentenceRecordReader();
			witer.initialize(fs);
			SentenceIterator iter = witer;

			TokenizerFactory t = new AnsjTokenzierFactory(true);
			t.setTokenPreProcessor(new AnsjTokenPreprocessor());

			AbstractCache<VocabWord> ac = new AbstractCache<VocabWord>();
			WeightLookupTable<VocabWord> table = new InMemoryLookupTable.Builder<VocabWord>()
					.vectorLength(vector_length).useAdaGrad(false)
					.cache(ac).build();

			LOG.info("Building model....");
			Word2Vec vec = new Word2Vec.Builder()
					.minWordFrequency(min_word_frequency)
					.iterations(1) // 每批次迭代次数
					.epochs(epochs) // 迭代次数
					.layerSize(layer_size).seed(seed)
					.windowSize(windows_size).iterate(iter)
					.tokenizerFactory(t).lookupTable(table)
					.vocabCache(ac).build();

			LOG.info("Fitting Word2Vec model....");
			vec.fit();
			LOG.info("Saving Word2Vec model...");
			WordVectorSerializer.writeWord2VecModel(vec, DL4JMODEL);
			return true;
		} catch (IOException | InterruptedException e1) {
			LOG.info("train model is fail");
			return false;
		}

	}

	// 训练文件预处理(e.g.分词、过滤、去停用词)
	public void processTrainFile(String lerningFilePath)
			throws FileNotFoundException, IOException {
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(lerningFilePath)))) {
			BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(
							new FileOutputStream(
									"H:\\wechat\\fdsplit.txt"),
							"utf-8"));
			String line;
			Parser p;
			IDocument idDocument;
			String result;
			while ((line = br.readLine()) != null) {
				if (line.split("\t").length > 7) {
					p = new DataBaseWechatParser();
					idDocument = p.parse(line);
				} else {
					p = new LineParser();
					idDocument = p.parse(line);
				}
				// 初步分词结果
				result = docSeg.segment(idDocument);
				// 分词过滤
				result = filterTerms(result);
				// 写入文件
				writer.write(result);
				writer.newLine();
			}
			writer.close();
		}

	}

	// 文档转换
	@Override
	public IDocumentVector transform(IDocument doc) {
		Map<String, Double> countWords = new HashMap<String, Double>();
		Vector docVector = new DenseVector(layer_size);
		List<IWord> words = new ArrayList<IWord>();
		// 分词
		String segResult = docSeg.segment(doc);
		// 词过滤
		segResult = filterTerms(segResult);
		for (String singword : segResult.split("\\s+")) {
			if (countWords.get(singword) == null)
				countWords.put(singword, Double.valueOf(1.0D));
			else {
				countWords.put(singword,
						Double.valueOf(countWords.get(singword) + 1));
			}
		}

		for (Map.Entry<String, Double> entry : countWords
				.entrySet()) {
			IWord word = new WordInWord2Vec((String) entry.getKey(),
					((Double) entry.getValue()).doubleValue());
			words.add(word);
		}
		for (IWord word : words) {
			Vector wordVec = new DenseVector(layer_size);
			if (queryWord(word) != null) {
				wordVec = queryWord(word);
			}
			weightedVec(wordVec, word.getWeight()); // 向量*词频
			wordToCount(docVector, wordVec); // 对文档中的所有词向量求和
		}
		IDocumentVector doucumentVector = new DocumentVectorImpl(
				doc.getID(), doc.getTag(), docVector);
		return doucumentVector;
	}

	// 文档集合转换
	@Override
	public List<IDocumentVector> transform(Iterator<IDocument> docs) {
		try {
			modelRWLock.readLock().lock();
			transformLock.readLock().lock();
			List<IDocumentVector> docVecs = new ArrayList<IDocumentVector>();
			while (docs.hasNext()) {
				docVecs.add(transform((IDocument) docs.next()));
			}
			return docVecs;
		} finally {
			transformLock.readLock().unlock();
			modelRWLock.readLock().unlock();
		}
	}

	// 查询词向量
	@Override
	public Vector queryWord(IWord word) {
		double[] wordVecArray = new double[layer_size];
		if (word2Vec.getWordVector(word.getWord()) != null)
			wordVecArray = word2Vec.getWordVector(word.getWord())
					.clone();
		Vector singleWordVec = new DenseVector(wordVecArray);
		return singleWordVec;
	}

	// 得到指定词的k个相似词
	@Override
	public Iterator<IWord> getTopKSimilar(int k, IWord word) {
		try{
			modelRWLock.readLock().lock();
		List<IWord> nearestWords = null;
		if (word2Vec != null) {
			nearestWords = new ArrayList<IWord>();
			Collection<String> nearestWord = word2Vec
					.wordsNearest(word.getWord(), k);
			if (nearestWord.isEmpty())
				return null;
			else {
				for (String sWord : nearestWord) {
					IWord i = new WordInWord2Vec(sWord);
					nearestWords.add(i);
				}
				LOG.info("size=" + nearestWords.size());
				return nearestWords.iterator();
			}
		}
		return null;
		}finally{
			modelRWLock.readLock().unlock();
		}
	}

	// 加载模型
	@Override
	public ModelStatus loadModelFromFile(File file) {
		try{
			modelRWLock.writeLock().lock();
		if (isModelExist() || file != null) {
			try {
				// 读模型有毒。虽然简化了模型大小，但是读模型有时会出现文件格式不对,读取模型和写入模型两个方法有待考究
				if (word2Vec == null) {
					if (file != null)
						word2Vec = WordVectorSerializer
								.readWord2VecModel(file);
					else {
						word2Vec = WordVectorSerializer
								.readWord2VecModel(DL4JMODEL);
					}
					loadModelStatus = new Dl4ModelStatus(1); // 加载模型成功
				} else {
					loadModelStatus = new Dl4ModelStatus(-1); // 模型已存在无需加载
				}
			} catch (Exception e) {
				loadModelStatus = new Dl4ModelStatus(-2); // 异常原因导致模型加载失败
			}
		} else {
			LOG.error("The model isn't exist");
			loadModelStatus = new Dl4ModelStatus(0); // 模型不存在
		}
		return loadModelStatus;
		}finally{
			modelRWLock.writeLock().unlock();
			LOG.info("加载模型过程释放锁");
		}

	}

	/*
	 * (non-Javadoc) 判断模型是否存在
	 * 
	 * @see cn.edu.bjtu.interfaces.ILearnerModel#isModelExist()
	 */
	@Override
	public boolean isModelExist() {
		if (JudgeFileExist(DL4JMODEL))
			return true;
		else
			return false;
	}

	// 对词向量加权
	private void weightedVec(Vector wordVec, double weight) {
		double[] wordVector = new double[wordVec.size()];
		for (int i = 0; i < wordVec.size(); i++) {
			wordVector[i] = (wordVec.get(i) * weight);
		}
		wordVec.assign(wordVector);
	}

	// 对文档中的词进行向量求和
	private void wordToCount(Vector docVector, Vector wordVec) {
		double[] docVec = new double[docVector.size()];
		for (int i = 0; i < docVector.size(); i++) {
			docVec[i] = (docVector.get(i) + wordVec.get(i));
		}
		docVector.assign(docVec);
	}

	// 词过滤
	protected String filterTerms(String segResult) {
		String result = segResult;
		for (WordsFilter filter : filtersSet) {
			result = filter.filter(result);
		}
		return result;
	}

	// 对文本直接进行向量准换
	public List<IDocumentVector> transformDoc(String doc) {
		try {
			modelRWLock.readLock().lock();
			transformLock.readLock().lock();
			IDocument idDocument = new Document(
					String.valueOf(this.cur.getAndIncrement()),
					"unknown", doc);
			List<IDocumentVector> listDocumentVec = new ArrayList<IDocumentVector>();
			IDocumentVector doucumentVector = transform(idDocument);
			listDocumentVec.add(doucumentVector);
			return listDocumentVec;
		} finally {
			transformLock.readLock().unlock();
			modelRWLock.readLock().unlock();
		}
	}

	// 获取指定路径下的需转换文件
	public Iterator<IDocument> getDoucument(String filePath)
			throws IOException {
		try {
			transformLock.readLock().lock();
			String line = "";
			Parser p1 = new DataBaseWechatParser();
			Parser p2 = new HiveWechatParser();
			Parser p3 = new LineParser();
			Parser p4 = new doscParser();
			Parser p5 = new FDDataParser();
			List<IDocument> documents = new ArrayList<IDocument>();
			if (JudgeFileExist(filePath)) {
				try (BufferedReader ber = new BufferedReader(
						new InputStreamReader(
								new FileInputStream(filePath),
								"UTF-8"))) {
					while ((line = ber.readLine()) != null) {
						IDocument document;
						if (line.split("\t").length > 8
								&& line.startsWith("c")) {
							document = p2.parse(line);
						} else if (line.split("\t").length > 8) {
							document = p1.parse(line);
						} else if (line.split("\t").length > 1) {
							document = p4.parse(line);
						} else
							if (line.split(" ")[1].endsWith("txt")) {
							document = p5.parse(line);
						} else {
							document = p3.parse(line);
						}
						if (document.getContent().replaceAll(" ", "")
								.length() > 0)
							documents.add(document);
					}
					LOG.info("文档数量=" + documents.size());
				}
				return documents.iterator();
			} else {
				return null;
			}
		} finally {
			transformLock.readLock().unlock();
		}
	}

	// 多文件
	public Iterator<IDocument> getDocuments(String filePath) {
		List<IDocument> documents = new ArrayList<IDocument>();
		List<File> listFile = new ArrayList<File>();
		File file = new File(filePath);
		if (file.isDirectory()) {
			FileUtil.getFilepath(file, listFile);
			for (File f : listFile) {
				try {
					Iterator<IDocument> temp = getDoucument(
							f.getAbsolutePath());
					while (temp.hasNext())
						documents.add((IDocument) temp.next());
				} catch (IOException e) {
					LOG.error(e.getMessage());
				}
			}
		} else if (file.isFile()) {
			try {
				Iterator<IDocument> temp = getDoucument(
						file.getAbsolutePath());
				while (temp.hasNext())
					documents.add((IDocument) temp.next());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LOG.info("文档总数量=" + documents.size());
		return documents.iterator();
	}

	/**
	 * docsToVecs modelIsExist writeToFile 三个方法用于测试使用
	 * 
	 * @param dir
	 * @throws IOException
	 */
	// 多文件转换（多文件单文档、多文件多文档）
	public void docsToVecs(String dir, String filePath)
			throws IOException {
		if (isModelExist()) {
			// loadModelFromFile();
			List<IDocumentVector> iter = null;
			File dirlist = new File(dir);
			DL4jWordVecModel doctran = DL4jWordVecModel.getInstance();
			if (dirlist.exists() && dirlist.isDirectory()) {
				iter = doctran.transform(doctran.getDocuments(dir));
			} else if (dirlist.exists() && dirlist.isFile()) {
				iter = doctran.transform(doctran.getDoucument(dir));
			}
			// 写入文件
			writeToFile(iter, filePath);
		}
	}

	// 判断文件是否存在
	private boolean JudgeFileExist(String filePath) {
		File file = new File(filePath);
		if (file.exists())
			return true;
		else
			return false;
	}

	// 写入文件
	public void writeToFile(List<IDocumentVector> iter,
			String filePath) throws IOException {
		try (BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filePath),
						"utf-8"))) {
			for (IDocumentVector doc : iter) {
				StringBuilder sb = new StringBuilder();
				sb.append(doc.getDocLabel() + " ");
				for (int i = 0; i < doc.getVector().size(); i++) {
					sb.append((i + 1) + ":" + doc.getVector().get(i))
							.append(" ");
				}
				String line = sb.toString();
				writer.write(line);
				writer.newLine();
			}
		}
	}

	public static void main(String[] args) {
		DL4jWordVecModel doctran = DL4jWordVecModel.getInstance();
		try {
			doctran.docsToVecs(doctran.TRANSFORM_DIR,
					"F:\\worksspace\\word2vecfile\\fdData\\trainfile\\tranform.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void fit(Iterator<IDocument> docs) {
		transform(docs);
	}

	/**
	 * 获取训练文件信息
	 * 
	 * @return
	 */
	public List<FileInfo> getFileInfo() {
		if (JudgeFileExist(TRAIN_FILE)) {
			List<FileInfo> listFileInfo = new ArrayList<FileInfo>();
			List<File> filelist = new ArrayList<File>();
			File path = new File(TRAIN_FILE);
			if (path.isDirectory()) {
				FileUtil.getFilepath(path, filelist);
				for (File file : filelist) {
					try (FileInputStream fis = new FileInputStream(
							file);) {
						if (file.exists()) {
							// 获取文件大小
							FileChannel fc = null;
							fc = fis.getChannel();
							long size = fc.size();
							String fileName = file.getName();// 获取文件名
							// 获取文件最近修改日期
							String modifyDate = ThreadLocalDateUtil
									.getDateFormat()
									.format(file.lastModified());
							// 作为对象返回
							FileInfo fileInfo = new FileInfo(fileName,
									size, modifyDate);
							listFileInfo.add(fileInfo);

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				if (path.exists()) {
					try (FileInputStream fis = new FileInputStream(
							path)) {
						// 获取文件大小
						FileChannel fc = null;
						fc = fis.getChannel();
						long size = fc.size();// 获取文件名
						String fileName = path.getName();
						// 获取文件最近修改日期
						String modifyDate = ThreadLocalDateUtil
								.getDateFormat()
								.format(path.lastModified());
						// 作为对象返回
						FileInfo fileInfo = new FileInfo(fileName,
								size, modifyDate);
						listFileInfo.add(fileInfo);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			return listFileInfo;
		} else {
			return null;
		}
	}

	/**
	 * @return the dL4JMODEL
	 */
	public String getDL4JMODEL() {
		return DL4JMODEL;
	}

	/**
	 * @param dL4JMODEL
	 *            the dL4JMODEL to set
	 */
	public void setDL4JMODEL(String dL4JMODEL) {
		DL4JMODEL = dL4JMODEL;
	}

	private boolean getFileSize(File file) throws Exception {
		// 获取文件大小
		FileChannel fc = null;
		try (FileInputStream fis = new FileInputStream(file)) {
			fc = fis.getChannel();
			long size = fc.size();
			if (size < 1024 * 1024 * 10 * 10) // 100M大小
				return true;
			else
				return false;
		}
	}

	public void dealBigFileTranform(String filePath,
			HttpServletResponse res) {
		DocumentVectorMessage documentVectorMessage;
		DL4jWordVecModel dlModel = DL4jWordVecModel.getInstance();
		// 修改处理方式，一行一行处理，处理后返回（2017-05-07）
		try (BufferedReader ber = new BufferedReader(
				new InputStreamReader(new FileInputStream(filePath),
						"UTF-8"))) {
			String line = "";
			Parser p1 = new DataBaseWechatParser();
			Parser p2 = new HiveWechatParser();
			Parser p3 = new LineParser();
			while ((line = ber.readLine()) != null) {
				IDocument document;
				documentVectorMessage = new DocumentVectorMessage(200,
						"success");
				if (line.split("\t").length > 8
						&& line.startsWith("c")) {
					document = p2.parse(line);
				} else if (line.split("\t").length > 8) {
					document = p1.parse(line);
				} else {
					document = p3.parse(line);
				}
				if (document.getContent().replaceAll(" ", "")
						.length() > 0) {
					List<IDocumentVector> temp = new ArrayList<IDocumentVector>();
					IDocumentVector docVec = dlModel
							.transform(document);
					temp.add(docVec);
					documentVectorMessage.setData(temp);
					String temp_result = JSONObject
							.fromObject(documentVectorMessage)
							.toString();
					FileUtil.OutPutStream(temp_result, res);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// 文件转换之后删除文件
			if (JudgeFileExist(filePath)) {
				new File(filePath).delete();
			}
		}
	}

	public ModelStatus getLoadModelStatus() {
		return loadModelStatus;
	}

	public void setLoadModelStatus(ModelStatus loadModelStatus) {
		this.loadModelStatus = loadModelStatus;
	}

	public Word2Vec getWord2Vec() {
		return word2Vec;
	}

	public void setWord2Vec(Word2Vec word2Vec) {
		this.word2Vec = word2Vec;
	}

}