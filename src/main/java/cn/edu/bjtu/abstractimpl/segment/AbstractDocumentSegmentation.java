package cn.edu.bjtu.abstractimpl.segment;

import cn.edu.bjtu.interfaces.WordSegResult;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.segment.DocumentSegmentation;

/**
 * 2017年4月23日
 */


/**
 * 这个类有什么存在必要么,当然有,目前DocumentSegmentation里面只有两个方法,
 * 这两个方法以后很可能不够用,如果分词类直接实现接口的话,DS接口里面只要添加一个方法,所有的分词类都会报错,
 * 所以加一具Abstract类,接口加方法之后只需要在加,这个类中实现,然后抛出异常即可,没有重写这些方法的分词类也不会报错,当调用没实现方法
 * 自然会扔出UOE异常
 * @author Alex
 *
 */
public class AbstractDocumentSegmentation implements DocumentSegmentation{

	/* (non-Javadoc)
	 * @see cn.edu.bjtu.wordseg.DocumentSegmentation#segment(cn.edu.bjtu.entity.IDocument)
	 */
	@Override
	public String segment(IDocument doc) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/* (non-Javadoc)
	 * @see cn.edu.bjtu.wordseg.DocumentSegmentation#segment(java.lang.String)
	 */
	@Override
	public String segment(String doc) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/* (non-Javadoc)
	 * @see cn.edu.bjtu.wordseg.DocumentSegmentation#segmentExtend(cn.edu.bjtu.entity.IDocument)
	 */
	@Override
	public WordSegResult segmentExtend(IDocument doc) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/* (non-Javadoc)
	 * @see cn.edu.bjtu.wordseg.DocumentSegmentation#segmentExtend(java.lang.String)
	 */
	@Override
	public WordSegResult segmentExtend(String doc) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

}
