package cn.edu.bjtu.abstractimpl;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import cn.edu.bjtu.general.math.Vector;
import cn.edu.bjtu.interfaces.ILearnerModel;
import cn.edu.bjtu.interfaces.ModelStatus;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.vector.IDocumentVector;
import cn.edu.bjtu.interfaces.wordfilters.IWord;

/**
 * @author Alex
 *
 */
public class AbstractLearnerModel implements ILearnerModel {

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.edu.bjtu.model.ILearnerModel#fit(java.util.Iterator)
	 */
	@Override
	public void fit(Iterator<IDocument> docs) {

		throw new UnsupportedOperationException("not implement");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cn.edu.bjtu.model.ILearnerModel#transform(cn.edu.bjtu.entity.IDocument)
	 */
	@Override
	public IDocumentVector transform(IDocument doc) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.edu.bjtu.model.ILearnerModel#transform(java.util.Iterator)
	 */
	@Override
	public List<IDocumentVector> transform(Iterator<IDocument> docs) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.edu.bjtu.model.ILearnerModel#queryWord(cn.edu.bjtu.model.IWord)
	 */
	@Override
	public Vector queryWord(IWord word) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.edu.bjtu.model.ILearnerModel#getTopKSimilar(int,
	 * cn.edu.bjtu.model.IWord)
	 */
	@Override
	public Iterator<IWord> getTopKSimilar(int k, IWord word) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see cn.edu.bjtu.model.ILearnerModel#isModelExist()
	 */
	@Override
	public boolean isModelExist() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("not implement");
	}

	/* (non-Javadoc)
	 * @see cn.edu.bjtu.interfaces.ILearnerModel#loadModelFromFile()
	 */
	@Override
	public ModelStatus loadModelFromFile(File file) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
