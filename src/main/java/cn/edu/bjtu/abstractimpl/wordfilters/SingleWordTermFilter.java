package cn.edu.bjtu.abstractimpl.wordfilters;

/**
 * 
 * @author LIYAO 单个字过滤
 *
 */
public class SingleWordTermFilter extends AbstractWordsFilter {
	protected boolean shouldRemove(String seg) {
		return seg.length() <= 1;
	}
}
