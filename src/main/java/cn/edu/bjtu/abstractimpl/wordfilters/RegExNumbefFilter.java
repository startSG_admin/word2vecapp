package cn.edu.bjtu.abstractimpl.wordfilters;

import java.util.regex.Pattern;

public class RegExNumbefFilter extends AbstractWordsFilter {
	public static Pattern NUM_PATTERN = Pattern
			.compile("[+-]?\\d+(?:\\.\\d+)?(?:e[+-]?\\d+)?");

	protected boolean shouldRemove(String seg) {
		return NUM_PATTERN.matcher(seg).find();
	}
}
