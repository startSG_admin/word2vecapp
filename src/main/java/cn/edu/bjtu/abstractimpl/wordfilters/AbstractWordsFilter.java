package cn.edu.bjtu.abstractimpl.wordfilters;

import java.util.regex.Pattern;

import cn.edu.bjtu.interfaces.wordfilters.WordsFilter;

public abstract class AbstractWordsFilter implements WordsFilter {
	private static Pattern SYMBOL_PATTERN = Pattern.compile(
			"[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]");

	protected abstract boolean shouldRemove(String paramString);

	public String filter(String seg) {
		StringBuffer bf = new StringBuffer();
		String[] segResult = seg.split("\\s+");
		for (int index = 0; index < segResult.length; index++) {
			if ((SYMBOL_PATTERN.matcher(segResult[index]).find())
					|| (shouldRemove(segResult[index]))) {
				segResult[index] = "";
			}
			bf.append(segResult[index] + " ");
		}
		return bf.toString();
	}
}
