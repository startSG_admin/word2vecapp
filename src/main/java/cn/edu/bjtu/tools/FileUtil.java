package cn.edu.bjtu.tools;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

public class FileUtil {
	public static void getFilepath(File filepath,List<File> listFile) {// 递归调用
		if (filepath != null) {// 判断对象是否为空
			if (filepath.isDirectory()) {// 如果是目录
				File f[] = filepath.listFiles();// 列出全部的文件
				if (f != null) {// 判断此目录能否列出
					for (int i = 0; i < f.length; i++) {
						getFilepath(f[i], listFile);// 因为给的路径有可能是目录，所以，继续判断
					}
				}
			} else {
					listFile.add(filepath);
			}
		}
	}
	  // 以流的形式返回
    public static void OutPutStream(String data, HttpServletResponse response) throws IOException {
	OutputStream outputStream = response.getOutputStream();// 获取OutputStream输出流
	response.setHeader("content-type", "text/html;charset=UTF-8");// 通过设置响应头控制浏览器以UTF-8的编码显示数据，如果不加这句话，那么浏览器显示的将是乱码
	/**
	 * data.getBytes()是一个将字符转换成字节数组的过程，这个过程中一定会去查码表，
	 * 如果是中文的操作系统环境，默认就是查找查GB2312的码表， 将字符转换成字节数组的过程就是将中文字符转换成GB2312的码表上对应的数字
	 */
	/**
	 * getBytes()方法如果不带参数，那么就会根据操作系统的语言环境来选择转换码表，如果是中文操作系统，那么就使用GB2312的码表
	 */
	byte[] dataByteArr = data.getBytes("UTF-8");// 将字符转换成字节数组，指定以UTF-8编码进行转换
	outputStream.write(dataByteArr);// 使用OutputStream流向客户端输出字节数组
    }
}
