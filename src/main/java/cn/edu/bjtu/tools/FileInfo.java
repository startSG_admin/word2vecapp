package cn.edu.bjtu.tools;
/**
 * 
 * @author LiYao
 * 2017-05-01
 *
 */

public class FileInfo {
	private String fileName;
	private long size;
	private String last_modified;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(String last_modified) {
		this.last_modified = last_modified;
	}

	public FileInfo(String fileName, long size,
			String last_modified) {
		super();
		this.fileName = fileName;
		this.size = size;
		this.last_modified = last_modified;
	}

}
