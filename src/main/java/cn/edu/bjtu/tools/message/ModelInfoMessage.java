package cn.edu.bjtu.tools.message;

import cn.edu.bjtu.tools.ModelInfo;

public class ModelInfoMessage extends Message {
	private ModelInfo modelInfo;

	public ModelInfoMessage(int status, String msg) {
		super(status, msg);
		// TODO Auto-generated constructor stub
	}

	public ModelInfo getModelInfo() {
		return modelInfo;
	}

	public void setModelInfo(ModelInfo modelInfo) {
		this.modelInfo = modelInfo;
	}


	public ModelInfoMessage() {
		super();
	}

}
