package cn.edu.bjtu.tools.message;

import java.util.List;

import cn.edu.bjtu.abstractimpl.documentvector.AbstractDocumentVectorMessage;
import cn.edu.bjtu.interfaces.vector.IDocumentVector;

public class DocumentVectorMessage extends AbstractDocumentVectorMessage{
	private List<IDocumentVector> data;
	public DocumentVectorMessage(int status, String msg) {
		super(status, msg);
		// TODO Auto-generated constructor stub
	}


	public List<IDocumentVector> getData() {
		return data;
	}

	public void setData(List<IDocumentVector> data) {
		this.data = data;
	}

}
