package cn.edu.bjtu.tools.message;

import java.util.List;

import cn.edu.bjtu.tools.FileInfo;

public class FileInfoMessage extends Message {
	
	private List<FileInfo> fileInfo;
	public FileInfoMessage(int status, String msg) {
		super(status, msg);
		// TODO Auto-generated constructor stub
	}
	public List<FileInfo> getFileInfo() {
		return fileInfo;
	}
	public void setFileInfo(List<FileInfo> fileInfo) {
		this.fileInfo = fileInfo;
	}



}
