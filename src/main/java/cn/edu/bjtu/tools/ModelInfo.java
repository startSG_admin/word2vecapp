package cn.edu.bjtu.tools;



public class ModelInfo {
	private String type ;
	private long time;
	private String modeName;

	public ModelInfo(long time, String modeName) {
		super();
		this.time = time;
		this.modeName = modeName;
	}




	public ModelInfo(String modeName,long time, String type) {
		super();
		this.modeName = modeName;
		this.type = type;
		this.time = time;
	}




	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public ModelInfo(String type) {
		this.type = type;
	}



	public long getTime() {
		return time;
	}



	public void setTime(long time) {
		this.time = time;
	}



	public String getModeName() {
		return modeName;
	}



	public void setModeName(String modeName) {
		this.modeName = modeName;
	}

	

}
