package cn.edu.bjtu.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.bjtu.word2vec.ApplicationConfig;


public class AppContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("Appkey", ApplicationConfig.getInstance());

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

}
