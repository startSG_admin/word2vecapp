/**
 * Tools for basic and advanced statistics: Estimators, Gamma functions, Beta functions, Probabilities,
 * Special integrals, etc.
 */
package cn.edu.bjtu.general.math.jet.stat;
