/**
 * Core interfaces for functions, comparisons and procedures on objects and primitive data types.
 */
package cn.edu.bjtu.general.math.function;
