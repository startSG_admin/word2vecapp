/**
 * Core base classes; Operations on primitive arrays such as sorting, partitioning and permuting.
 */
package cn.edu.bjtu.general.math;
