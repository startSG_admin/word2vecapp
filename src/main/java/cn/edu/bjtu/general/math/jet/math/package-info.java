/**
 * Tools for basic and advanced mathematics: Arithmetics and Algebra, Polynomials and Chebyshev series, Bessel and Airy
 * functions, Function Objects for generic function evaluation, etc.
 */
package cn.edu.bjtu.general.math.jet.math;
