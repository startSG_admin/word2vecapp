package cn.edu.bjtu.interfaces;



import java.io.File;
import java.util.Iterator;
import java.util.List;

import cn.edu.bjtu.general.math.Vector;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.vector.IDocumentVector;
import cn.edu.bjtu.interfaces.wordfilters.IWord;
/**
 * 集成LDA RNN Word2Vec请实现这个接口
 * @author Alex
 *
 */
public interface ILearnerModel {
	
	void fit(Iterator<IDocument> docs);
	IDocumentVector transform(IDocument doc);
	List<IDocumentVector> transform(Iterator<IDocument> docs);
	Vector queryWord(IWord word);
	Iterator<IWord> getTopKSimilar(int k,IWord word);
	
	boolean isModelExist();
	/**
	 * @return
	 */
	ModelStatus loadModelFromFile(File file);
	
	
}
