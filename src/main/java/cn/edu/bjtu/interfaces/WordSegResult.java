package cn.edu.bjtu.interfaces;

/**
 * 为什么要有这个类，
 * 考虑以下这种情况
 * 目前分词方式有两种，
 * 对于ansj返回的结果是org.ansj.domain.Result类
 * 对于smartchinese类，其返回的结果是 一系列的org.apache.lucene.analysis.tokenattributes。CharTermAttributeImpl
 * WordSegmentation类中，对于每种分词都要添加一个方法返回对应结果，这有多少种分词至少就要加多少个方法
 * 接口不应该经常变动的，添加之后对于子类的影响太大。
 * 
 * @author Alex
 *
 */
public interface WordSegResult {
	/**
	 * 实现这个类方法,最终肯定要返回以空格分割的词语
	 * @return
	 */
	String toString();
}
