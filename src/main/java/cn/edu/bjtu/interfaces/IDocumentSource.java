package cn.edu.bjtu.interfaces;

import java.util.Iterator;

import cn.edu.bjtu.interfaces.document.IDocument;


public interface IDocumentSource extends Iterator<IDocument> {
}
