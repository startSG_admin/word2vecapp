package cn.edu.bjtu.interfaces.document;

/**
 * 表示一个文档,也就是一条微信.
 * 与微信不同的是getID应该返回微信的唯一标识,这个标识是微信前4个字段的组合.
 * 对于未标记的文档,getTag返回空就好
 * content就是文档内容了
 * Wechat类与数据库表ORM所以度到了Pojo里面
 * 以迭代器模型读取所有的文本文件并返回IDocument
 * @see util.docparser.impl.FileDocumentSource
 * @author Alex
 *
 */
public interface IDocument {
	public String getID();
	public String getTag();
	public String getContent();
}
