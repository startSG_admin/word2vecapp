package cn.edu.bjtu.interfaces.vector;

import cn.edu.bjtu.general.math.Vector;


/**
 * @author Alex
 *
 */
public interface IDocumentVector {
	String getDocId();
	String getDocLabel();
	Vector getVector();
}
