package cn.edu.bjtu.interfaces.parser;

import cn.edu.bjtu.interfaces.document.IDocument;

/**
 * @author Alex
 *
 */
public interface Parser {
	public IDocument parse(String line); 
}
