package cn.edu.bjtu.interfaces.segment;

import cn.edu.bjtu.interfaces.WordSegResult;
import cn.edu.bjtu.interfaces.document.IDocument;

/**
 * 分词接口,分词模块的类都应该实现这个接口.
 * 结合以前的代码,目前应该有两种分词,一种是lucence中的smartchinese
 * 另外一种是ansj提供的分词
 * @author Alex
 *
 */
public interface DocumentSegmentation {
	/**
	 * 这个接口接受一个文件,返回一个字符串,字符串是由空格分割的词组.
	 * 返回以空格分割的字符串有这样好处,可以兼容一些以空格为划分的英文文本处理方法
	 * @param doc 要被分词处理的doc
	 * @return
	 */
	String segment(IDocument doc);
	
	String segment(String doc);
	
	WordSegResult segmentExtend(IDocument doc);
	WordSegResult segmentExtend(String doc);
	
	
}
