package cn.edu.bjtu.interfaces.wordfilters;

/**
 * 表示一个词，可以添加词性，词位置，或者其他和词有关的特征
 * @author Alex
 *
 */
public interface IWord {
	String getWord();
	double getWeight();
}
