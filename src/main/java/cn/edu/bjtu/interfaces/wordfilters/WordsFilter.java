package cn.edu.bjtu.interfaces.wordfilters;
/**
 * 
 * @author LIYAO
 * 用于对分词的过滤
 *
 */
public abstract interface WordsFilter{
	
   String filter(String paramString);
}

