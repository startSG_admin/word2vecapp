/**
 * 2017年4月20日
 */
package cn.edu.bjtu.classimpl.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import cn.edu.bjtu.interfaces.IDocumentSource;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.parser.Parser;
import cn.edu.bjtu.tools.ReflectionUtils;


/**
 * @author Alex
 *
 */
public class FileDocumentSource implements IDocumentSource,FileVisitor<Path>{

	Parser wechatParser;
	List<Path> paths = new ArrayList<>(10);
	public FileDocumentSource(File dir) {
		wechatParser = (Parser) ReflectionUtils.getInstance("cn.edu.bjtu.classimpl.parser.LineParser");
		init(dir);
	}
	public FileDocumentSource(File dir,Parser p) {
		wechatParser = p;
		init(dir);
	}
	
	private void init(File dir){
		try {
			Files.walkFileTree(Paths.get(dir.toURI()), this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		nextBr();
	}
	IDocument cur = null;
	BufferedReader br = null;
	private void nextLine(){
		if(br == null){
			cur = null;
			return ;
		}
		try {
			String line = br.readLine();		
			cur = wechatParser.parse(line);
		} catch (IOException e) {
			cur = null;
			e.printStackTrace();
		}
		
	}
	private boolean nextBr(){
		//关闭当前
		closeBrQuite();
		while(paths.size()>0){
			try {
				br = new BufferedReader(new FileReader(paths.remove(0).toFile()));
				return true;
			} catch (FileNotFoundException e) {
				br = null;
				e.printStackTrace();			
			}
		}
		return false;
	}
	private void closeBrQuite(){
		try{if(br!=null){br.close();
			System.out.println("closing "+br);br = null;}}catch(Exception e){e.printStackTrace();}
	}
	@Override
	public boolean hasNext() {		
		nextLine();
		if(cur == null){
			nextBr();
			nextLine();
		}
		return cur != null;
	}

	public IDocument next() {
		return cur;
	}

	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		//不遍历*nix系统下的隐藏文件
		if(dir.startsWith(".")){
			return FileVisitResult.SKIP_SUBTREE;
		}
		return FileVisitResult.CONTINUE;
	}
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		// TODO Auto-generated method stub
		if(file.startsWith(".") ||  !file.endsWith("arff") || !file.endsWith("ARFF")){
//			return FileVisitResult.SKIP_SUBTREE;
		}
		paths.add(file);
		return FileVisitResult.CONTINUE;
	}
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		// TODO Auto-generated method stub
		return FileVisitResult.CONTINUE;
	}
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		// TODO Auto-generated method stub
		return FileVisitResult.CONTINUE;
	}

	public static void main(String args[]){
		FileDocumentSource fds = new FileDocumentSource(new File("D:\\textdata\\data"));
		while(fds.hasNext()){
			System.out.println(fds.next());
		}
		
	}
	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}
