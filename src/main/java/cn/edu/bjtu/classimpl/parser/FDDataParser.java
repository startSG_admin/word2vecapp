package cn.edu.bjtu.classimpl.parser;

import cn.edu.bjtu.classimpl.documententity.Document;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.parser.Parser;

import java.util.concurrent.atomic.AtomicLong;

public class FDDataParser implements Parser {
    AtomicLong cur = new AtomicLong(0L);

    public IDocument parse(String line) {
	String parts[] = line.split(" ");
	String label = parts[0];
	String id = parts[1];
	String content = line.substring(line.indexOf("txt")+1);
	return new Document(id, label,content);
    }
}
