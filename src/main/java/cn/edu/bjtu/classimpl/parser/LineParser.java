package cn.edu.bjtu.classimpl.parser;

import cn.edu.bjtu.classimpl.documententity.Document;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.parser.Parser;

import java.util.concurrent.atomic.AtomicLong;

public class LineParser implements Parser {
    AtomicLong cur = new AtomicLong(0L);

    public IDocument parse(String line) {
	if (line == null)
	    return null;
	return new Document(String.valueOf(this.cur.getAndIncrement()), "unknow",line);
    }
}
