package cn.edu.bjtu.classimpl.parser;

import cn.edu.bjtu.classimpl.documententity.Document;
import cn.edu.bjtu.interfaces.document.IDocument;
import cn.edu.bjtu.interfaces.parser.Parser;

/**
 * @author Alex
 *
 */
public class doscParser implements Parser{

	@Override
	public IDocument parse(String line) {
		if(line == null) return null;
		String parts [] = line.split("\t");
		String id ="unknown"; 
		String label = parts[0];
		String content = parts[1];
		return new Document(id,label,content);
	}

}
