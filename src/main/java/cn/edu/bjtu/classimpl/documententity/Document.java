package cn.edu.bjtu.classimpl.documententity;

import cn.edu.bjtu.interfaces.document.IDocument;

/**
 * @author Alex
 *
 */
public class Document implements IDocument{
	public String tag;
	public String content;
	public String id;
	public Document(){
		
	}
	public Document(String id ,String tag,String content){
		this.tag = tag;
		this.content = content;
		this.id = id;
	}
	//用做容器的key的话,注意重写equals 和   hashCode方法
	/* (non-Javadoc)
	 * @see pojo.weichatlab.model.IDocument#getID()
	 */
	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return id;
	}
	/* (non-Javadoc)
	 * @see pojo.weichatlab.model.IDocument#getTag()
	 */
	@Override
	public String getTag() {
		// TODO Auto-generated method stub
		return tag;
	}
	/* (non-Javadoc)
	 * @see pojo.weichatlab.model.IDocument#getContent()
	 */
	@Override
	public String getContent() {
		// TODO Auto-generated method stub
		return content;
	}
	public String toString(){
		return id+"\t"+content;
	}
	
	
}
