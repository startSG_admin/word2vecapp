package cn.edu.bjtu.classimpl.documentvec;


import cn.edu.bjtu.abstractimpl.documentvector.AbstractDocumentVector;
import cn.edu.bjtu.general.math.Vector;

public class DocumentVectorImpl extends AbstractDocumentVector {
	private String docId;
	private String docLabel;
//	private Vector mVector = null;
	private Vector vec ;

	public DocumentVectorImpl(String docId, String docLabel	,Vector vec) {
		this.docId = docId;
		this.docLabel = docLabel;
		this.vec = vec;
	}

	public String getDocId() {
		return this.docId;
	}

	public String getDocLabel() {
		return this.docLabel;
	}


	@Override
	public Vector getVector() {
		return vec;
	}
}
