package cn.edu.bjtu.classimpl;

import cn.edu.bjtu.abstractimpl.AbstractModelStatus;
//建造模型信息

public class Dl4ModelStatus extends AbstractModelStatus {

    private int status;
    private long buildModelTime;

    public Dl4ModelStatus(int status) {
	this.status = status;
    }

    /**
	 * @param status
	 * @param buildModelTime
	 */
	public Dl4ModelStatus(int status, long buildModelTime) {
		this.status = status;
		this.buildModelTime = buildModelTime;
	}

	@Override
    public int getstatus() {
	// TODO Auto-generated method stub
	return status;
    }

	@Override
	public long getBuildTime() {
		// TODO Auto-generated method stub
		return buildModelTime;
	}
	
}
