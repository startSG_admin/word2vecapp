/**
 * 2017年5月21日
 */
package cn.edu.bjtu.core;

/**
 * 线程安全
 * @author Alex
 *
 */
public class SentenceDetect extends LoggerSupport{
	private SentenceDetect(){}
	private static SentenceDetect inst = new SentenceDetect();
	public static SentenceDetect get(){
		return inst;
	}
	public boolean isFullfield(String s){
		if(!shouldGoOn(s)){
//			logger.info("被过滤的文档:{}",s);
			return false;
		}else{
			return true;
		}
	}

	// 根据Unicode编码完美的判断中文汉字和符号
	private boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
			return true;
		}
		return false;
	}

	private boolean isEnglish(char c) {
		return (c>='a' && c<='z') || (c>='A' && c<='Z');
	}
	
	// 长度判断 语种判断
	private boolean shouldGoOn(String strName) {
		if(strName.length() < 25)
			return false;
		int validLen = 0;
		for (int i = 0; i < strName.length(); i++) {
			if (isEnglish(strName.charAt(i)) || isChinese(strName.charAt(i)))
				validLen++;
		}
		if (validLen * 1.0 / strName.length() > 0.35)
			return true;
		else
			return false;
	}
}
