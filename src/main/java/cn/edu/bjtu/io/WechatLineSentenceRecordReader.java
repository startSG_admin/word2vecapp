/**
 * 2017年5月17日
 */
package cn.edu.bjtu.io;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.ansj.domain.Result;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.datavec.api.records.reader.impl.LineRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.writable.Writable;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentencePreProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.bjtu.core.SentenceDetect;

/**
 * 读句子
 * 
 * @author Alex
 *
 */
public class WechatLineSentenceRecordReader extends LineRecordReader
		implements SentenceIterator {
	private static final long serialVersionUID = -4599612791106295846L;
	protected Logger log = LoggerFactory.getLogger(this.getClass());
	protected ToAnalysis ta = new ToAnalysis();
	int seq = 8;

	public WechatLineSentenceRecordReader() {
		this(8);
	}

	public WechatLineSentenceRecordReader(int contentSeq) {
		seq = 8;
	}

	@Override
	public String nextSentence() {
		List<Writable> l = this.next();
		String line = l.get(0).toString();
		while (!canHandle(line) && hasNext()) {
			List<Writable> temp = next();
			line = temp.get(0).toString();
		}
		if (!canHandle(line)) {
			log.info("error foramt at {},of split {} ,content {}",
					this.lineIndex, this.splitIndex, line);
		}
		return line;
	}

	@Override
	public void finish() {
		try {
			super.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private boolean canHandle(String strName) {
		return SentenceDetect.get().isFullfield(strName);
	}

	@Override
	public SentencePreProcessor getPreProcessor() {
		return null;
	}

	@Override
	public void setPreProcessor(SentencePreProcessor preProcessor) {
	}

}
