/**
 * 2017年5月23日
 */
package org.bjtu.word2vec.gui;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Alex
 *
 */
public class PoolSupport extends SelfAdjustJFrame{
	
	private static final long serialVersionUID = 7529657283645136055L;
	protected ExecutorService es = Executors.newFixedThreadPool(5);
	
}
