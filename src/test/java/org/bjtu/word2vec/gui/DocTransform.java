/**
 * 2017年5月24日
 */
package org.bjtu.word2vec.gui;

import java.awt.GridBagConstraints;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.graph.GraphVertex;
import org.deeplearning4j.nn.graph.ComputationGraph;

import cn.edu.bjtu.interfaces.vector.IDocumentVector;
import cn.edu.bjtu.interfaces.wordfilters.IWord;
import cn.edu.bjtu.model.dl4jword2vec.DL4jWordVecModel;
import cn.edu.bjtu.model.word2vec.util.WordInWord2Vec;

/**
 * @author Alex
 *
 */
public class DocTransform extends SelfAdjustJFrame {
	private static final long serialVersionUID = 3163183630447977150L;
	JTextArea text;
	JButton transform;
	private DL4jWordVecModel model = DL4jWordVecModel.getInstance();
	private ThreadFactory _tf = new ThreadFactory() {
		AtomicInteger at = new AtomicInteger(0);

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setName("DocTransform-Event-Handler-Pool-"
					+ at.getAndIncrement());
			return t;
		}
	};
	private ExecutorService eventHandler = Executors
			.newFixedThreadPool(3, _tf);
	public DocTransform(JFrame parent) {
		setTitle("单文档转换");
		setSize(800,570);
		initUI();
		initEventHandler();
		setVisible(true);
	}
	private void initUI(){
		transform =  new JButton("转换");
		text = new JTextArea();
		text.setLineWrap(true);
		addComp(text);
		addComp(transform);
		gbl.setConstraints(text, new GBC(0,13).setGridWidth(6).setWx(7).setWy(1)
				.setFill(GridBagConstraints.BOTH));
		gbl.setConstraints(transform, new GBC(7, 13));
		
	}
	private void initEventHandler(){
		transform.addActionListener((e) -> {
			logger.info("Event");
			eventHandler.execute(() -> {
				List<IDocumentVector> docVecs = null;
				try {
					docVecs = model.transformDoc(text.getText().toString());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				StringBuilder sb = new StringBuilder();
				for(IDocumentVector docvec:docVecs){
					sb.append(docvec.getDocId()+"\t"+docvec.getDocLabel()+"\r\n");
					for(int i=0 ;i<docvec.getVector().size();i++){
						sb.append(docvec.getVector().get(i)+" ");
					}
					sb.append("\r\n");
				}	
				text.setText(sb.toString());
			});
		});
	}
}
