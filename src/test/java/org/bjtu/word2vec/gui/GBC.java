/**
 * 2017年5月23日
 */
package org.bjtu.word2vec.gui;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * @author Alex
 *
 */
public class GBC extends GridBagConstraints{
	private static final long serialVersionUID = 8792565500914727287L;
	public GBC(int gridx, int gridy) {
	   super();
	   this.gridx = gridx;
	   this.gridy = gridy;
	}
	public GBC setGridWidth(int gw){
		this.gridwidth = gw;
		return this;
	}
	public GBC setGridHeight(int gh){
		this.gridheight = gh;
		return this;
	}
	public GBC setWx(double x){
		this.weightx = x;
		return this;
	}
	public GBC setWy(double y){
		this.weighty = y;
		return this;
	}
	public GBC setAnchor(int a){
		this.anchor = a;
		return this;
	}
	public GBC setFill(int f){
		this.fill = f;
		return this;
	}
	public GBC setInst(Insets in){
		this.insets = in;
		return this;
	}
	public GBC setPadX(int x){
		this.ipadx = x;
		return this;
	}
	public GBC setPadY(int y){
		this.ipady = y;
		return this;
	}
}
