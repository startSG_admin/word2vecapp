/**
 * 2017年5月23日
 */
package org.bjtu.word2vec.gui;

import java.awt.Component;

import javax.swing.JPanel;

/**
 * @author Alex
 *
 */
public class SelfAdjustJPanel extends JPanel {

	private static final long serialVersionUID = -7103432036822825462L;
	protected <T extends Component> T addComp(T t){
		super.add(t);
		return t;
	}
}
