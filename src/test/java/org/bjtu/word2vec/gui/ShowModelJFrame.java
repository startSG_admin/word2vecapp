/**
 * 2017年5月24日
 */
package org.bjtu.word2vec.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.nn.api.Layer;
import org.deeplearning4j.nn.conf.graph.GraphVertex;
import org.deeplearning4j.nn.graph.ComputationGraph;

import cn.edu.bjtu.model.dl4jword2vec.DL4jWordVecModel;

/**
 * @author Alex
 *
 */
public class ShowModelJFrame extends SelfAdjustJFrame {
	private DL4jWordVecModel model = DL4jWordVecModel.getInstance();
	private static final long serialVersionUID = 3163183630447977150L;
	public ShowModelJFrame(JFrame parent) {
		setTitle("模型参数");
		setSize(400,300);
		initUI();
		initEventHandler();
		setVisible(true);
	}
	private void initUI(){
		Object [] colName = new String[]{"参数名","参数值"};
		Word2Vec w2v = model.getWord2Vec();
		
		int size = w2v.getLayerSize();
		int minFreq = w2v.getMinWordFrequency();
		int wordSize = w2v.getVocab().words().size();
		int windowSize = w2v.getWindow();
		String [][]  data = new String[][]{
			new String[]{"词向量长度",String.valueOf(size)},
			new String[]{"最小词频",String.valueOf(minFreq)},
			new String[]{"词表大小",String.valueOf(wordSize)},
			new String[]{"窗口大小",String.valueOf(windowSize)},
		};
		
		JLabel jl1 = addComp(new JLabel("Word2Vec参数"));
		JTable jt = addComp(new JTable(data,colName));
	/*	
		Deep4jNetworkWrapper cg = model.getNet();
		Layer [] layers = cg.getLayers();
//		Map<String, GraphVertex> vertexMap = cg.get.getVertices();
		List<String[]> dataList =  new ArrayList<String[]>(5);
		dataList.add(new String[]{"网络层数",String.valueOf(layers.length)});
		
		
//		Set<String> keys = vertexMap.keySet();
//		for(String s:keys){
//			dataList.add(new String[]{s,vertexMap.get(s).toString()});
//		}
		data = dataList.toArray(new String[dataList.size()][]);
		JLabel jl2 = addComp(new JLabel("CNN结构"));
		JTable jt2 = addComp(new JTable(data,colName));
		gbl.setConstraints(jl1, new GBC(0,0).setFill(GBC.BOTH).setWx(1));
		gbl.setConstraints(jt, new GBC(0,1).setFill(GBC.BOTH).setWx(1));
		gbl.setConstraints(jl2, new GBC(0,2).setFill(GBC.BOTH).setWx(1));
		gbl.setConstraints(jt2, new GBC(0,3).setFill(GBC.BOTH).setWx(1));
		*/
		
	}
	private void initEventHandler(){
		
	}
}
