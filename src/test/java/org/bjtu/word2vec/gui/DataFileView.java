/**
 * 2017年5月23日
 */
package org.bjtu.word2vec.gui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.datavec.api.split.FileSplit;


import org.deeplearning4j.berkeley.Pair;

/**
 * @author Alex
 *
 */
public class DataFileView extends PoolSupport{

	private static final long serialVersionUID = -5042065101863787407L;
	File fileToOpen = null;
	JFrame parent = null;
	JTextArea area ;
	JTextArea stat;
	
	public DataFileView(JFrame parent,File f) {
		this.parent = parent;
		fileToOpen = f;
		this.setLayout(gbl);
		init();
		setSize(1024, 768);
		setVisible(true);
		setTitle("文件查看");
	}
	private void init(){
		SelfAdjustJPanel jp = new SelfAdjustJPanel();
		JLabel jl = addComp(new JLabel(fileToOpen.getAbsolutePath()));
		jp.setLayout(new FlowLayout());
		jp.addComp(new JButton("打开文件")).addActionListener((e)->{
			es.execute(new ReadContent());
		});
		jp.addComp(new JButton("统计信息")).addActionListener((e)->{
			//es.execute(new StatContent());
		});;
		jp.addComp(new JLabel("正则表达式:"));
		JTextField jtf = jp.addComp(new JTextField("输入用于过滤的正则表达式          "));
		
		jp.addComp(new JButton("过滤")).addActionListener((e)->{
			System.out.println(jtf.getText());
		});
		addComp(jp);
		
		SelfAdjustJPanel jp2 = addComp(new SelfAdjustJPanel());
		jp2.setLayout(new FlowLayout());
		jp2.addComp(new JLabel("排除含有类标的文档"));
		jp2.addComp(new JTextField("这里输入待删除的类标,使用英文,隔开"));
		jp2.addComp(new JButton("保存文件")).addActionListener((e)->{
			
		});;
		
		
		
		area = new JTextArea();
		JScrollPane jsp = new JScrollPane(area);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		addComp(jsp);
		stat = new JTextArea();
		stat.setLineWrap(true);
		JScrollPane jsp2 = new JScrollPane(stat);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		addComp(jsp2);
		gbl.setConstraints(jl,new GBC(0,0).setFill(GridBagConstraints.BOTH));
		gbl.setConstraints(jp, new GBC(0,1).setFill(GridBagConstraints.BOTH));
		gbl.setConstraints(jp2, new GBC(0,2).setFill(GridBagConstraints.BOTH));
		gbl.setConstraints(jsp, new GBC(0,3).setGridHeight(10).setGridWidth(10).setWx(1).setWy(1).setFill(GridBagConstraints.BOTH));
		gbl.setConstraints(jsp2, new GBC(0,13).setGridHeight(10).setGridWidth(10).setWx(1).setWy(1).setFill(GridBagConstraints.BOTH));
	}

	class ReadContent implements Runnable{
		@Override
		public void run() {
			try(BufferedReader br = new BufferedReader(new FileReader(fileToOpen))){
				String line = null;
				while((line = br.readLine())!=null){
					area.append(line);
					area.append("\r\n");
				}
			}catch(Exception e){
				logger.info(e.getMessage());
			}
		}
	}
	/*class StatContent implements Runnable{
		@Override
		public void run() {
			try {
				Map<String,Integer> res = new HashMap<String,Integer>();
				MultiLabelTrainCNNSentenceProvider p = new MultiLabelTrainCNNSentenceProvider();
				p.initialize(new FileSplit(fileToOpen));
				while(p.hasNext()){
					Pair<String,String> sen = p.nextSentence();
					String l = sen.getSecond();
					String [] laels = l.split(",");
					Arrays.asList(laels).forEach(x->{
						int r = res.getOrDefault(x, 0);
						res.put(x, r+1);
					});
				}
				res.forEach((k,v)->{
					stat.append(String.format("类标:%s,次数:%d", k,v));
					stat.append("\r\n");
				});
				p.close();
			} catch (IOException | InterruptedException e) {
				stat.setText("打开文件格式不正确");
				e.printStackTrace();
			}
			
			
		}
	}*/

	
}
