/**
 * 2017年5月23日
 */
package org.bjtu.word2vec.gui;

import java.awt.Component;
import java.awt.GridBagLayout;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alex
 *
 */
public class SelfAdjustJFrame extends JFrame{
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final long serialVersionUID = -8145255841332564638L;
	protected GridBagLayout gbl = new GridBagLayout();
	public SelfAdjustJFrame() {
		setLayout(gbl);
	}
	protected <T extends Component> T addComp(T t){
		super.add(t);
		return t;
	}
}
