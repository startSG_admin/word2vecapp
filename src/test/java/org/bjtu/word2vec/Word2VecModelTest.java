package org.bjtu.word2vec;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.datavec.api.split.FileSplit;
import org.deeplearning4j.models.embeddings.WeightLookupTable;
import org.deeplearning4j.models.embeddings.inmemory.InMemoryLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.AbstractCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.bjtu.io.LineSentenceRecordReader;
import cn.edu.bjtu.io.WechatLineSentenceRecordReader;
import cn.edu.bjtu.tokenization.AnsjTokenPreprocessor;
import cn.edu.bjtu.tokenization.AnsjTokenzierFactory;

public class Word2VecModelTest {
	private static Logger LOG = LoggerFactory
			.getLogger(Word2VecModelTest.class);
	private  String trainFilePath = "E:\\LiYao\\fd\\fddatatest\\";
	private  String modelPath = "E:\\LiYao\\fd\\mymodel\\";
	@Test
	public void builderModel() {
		// 设置模型运行参数
		int min_word_frequency = 5;
		int window_size = 5;
		int layer_size = 100;
		int seed = 42;
		int epochs = 1;
		modelPath +="fdModel_"+layer_size+"_"+epochs+".txt";
		try {

			LOG.info("loading trainFile...");
			FileSplit fs = new FileSplit(new File(trainFilePath));
			LineSentenceRecordReader witer = new LineSentenceRecordReader();
			witer.initialize(fs);
			SentenceIterator iter = witer;
//			while (witer.hasNext()) {
//				LOG.info(witer.nextSentence());	
//			}

			
			TokenizerFactory t = new AnsjTokenzierFactory(false);
			t.setTokenPreProcessor(new AnsjTokenPreprocessor());
			
			AbstractCache<VocabWord> ac = new AbstractCache<VocabWord>();
			WeightLookupTable<VocabWord> table = new InMemoryLookupTable.Builder<VocabWord>()
					.vectorLength(layer_size).useAdaGrad(false).cache(ac)
					.build();

			LOG.info("Building model....");
			Word2Vec vec = new Word2Vec.Builder()
					.minWordFrequency(min_word_frequency)
					.iterations(1) // 每批次迭代次数
					.epochs(epochs) // 迭代次数
					.layerSize(layer_size)//词向量
					.seed(seed)
					.windowSize(window_size)
					.iterate(iter)
					.tokenizerFactory(t)
					.lookupTable(table)
					.vocabCache(ac).build();

			LOG.info("Fitting Word2Vec model....");
			vec.fit();
			LOG.info("save model");
			WordVectorSerializer.writeWordVectors(vec, modelPath);
			Word2Vec wv = WordVectorSerializer.readWord2VecModel(modelPath);
			LOG.info("Training finished");
		} catch (IOException | InterruptedException e1) {
			LOG.info("train model is fail");
		}

	}
}
